-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2017 at 01:02 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `primarie`
--

-- --------------------------------------------------------

--
-- Table structure for table `angajati`
--

CREATE TABLE `angajati` (
  `nume` varchar(20) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `cnp` varchar(13) NOT NULL,
  `telefon` varchar(11) NOT NULL,
  `nrdep` int(11) NOT NULL,
  `preg` varchar(20) NOT NULL,
  `vechime` int(11) NOT NULL,
  `angajare` varchar(20) NOT NULL,
  `functie` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `angajati`
--

INSERT INTO `angajati` (`nume`, `prenume`, `cnp`, `telefon`, `nrdep`, `preg`, `vechime`, `angajare`, `functie`) VALUES
('asdasd', 'asdasd', 'asdasd', '12345678901', 123, '123', 123, '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `audienta`
--

CREATE TABLE `audienta` (
  `nume` varchar(20) NOT NULL,
  `prenume` varchar(50) NOT NULL,
  `data` varchar(10) NOT NULL,
  `ora` varchar(10) NOT NULL,
  `cnp` varchar(13) NOT NULL,
  `telefon` varchar(10) NOT NULL,
  `subiect` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audienta`
--

INSERT INTO `audienta` (`nume`, `prenume`, `data`, `ora`, `cnp`, `telefon`, `subiect`) VALUES
('g', 'g', 'g', 'g', 'g', 'g', 'g');

-- --------------------------------------------------------

--
-- Table structure for table `departament`
--

CREATE TABLE `departament` (
  `numedep` varchar(20) NOT NULL,
  `nrdep` int(11) NOT NULL,
  `cnpsef` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'primar', '7cfb5a9794dca40d6089fc7125bc5721d8495ba1'),
(2, 'abc', 'e4f19dcc3f06c635128b1f91714549e0c408f159'),
(3, '123', 'e4f19dcc3f06c635128b1f91714549e0c408f159');

-- --------------------------------------------------------

--
-- Table structure for table `webchat_lines`
--

CREATE TABLE `webchat_lines` (
  `id` int(10) UNSIGNED NOT NULL,
  `author` varchar(16) NOT NULL,
  `gravatar` varchar(32) NOT NULL,
  `text` varchar(255) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `webchat_users`
--

CREATE TABLE `webchat_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(16) NOT NULL,
  `gravatar` varchar(32) NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `webchat_users`
--

INSERT INTO `webchat_users` (`id`, `name`, `gravatar`, `last_activity`) VALUES
(5, 'zsdasdasd', '621fd05334bf41564428ef0f3231ede4', '2017-05-31 10:46:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angajati`
--
ALTER TABLE `angajati`
  ADD PRIMARY KEY (`cnp`);

--
-- Indexes for table `audienta`
--
ALTER TABLE `audienta`
  ADD PRIMARY KEY (`nume`);

--
-- Indexes for table `departament`
--
ALTER TABLE `departament`
  ADD PRIMARY KEY (`nrdep`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webchat_lines`
--
ALTER TABLE `webchat_lines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ts` (`ts`);

--
-- Indexes for table `webchat_users`
--
ALTER TABLE `webchat_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `last_activity` (`last_activity`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `webchat_lines`
--
ALTER TABLE `webchat_lines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `webchat_users`
--
ALTER TABLE `webchat_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
